package com.im.server.general.common.bean.system;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * 
 * date 2018-07-04 16:12:57<br>
 * description 菜单信息
 * 
 * @author XiaHui<br>
 * @since
 */
@Entity(name = "m_menu")
public class Menu {
	
	@Id
	@Column(name = "id", unique = true, nullable = false, length = 40)
	private String id;// 菜单id
	private String superId;// 上级菜单id
	private String name;// 菜单名称
	private String flag;// 是否启用 1：启用 0：停用
	private String introduce;// 功能描述
	private Date createTime;// 创建时间
	private int rank;// 菜单排序
	private int type;// 菜单类别 0：菜单 1：按钮

	//
	public static final int type_menu = 0;
	public static final int type_function = 1;

	// ////非持久化字段
	public static final String flag_disable = "0";
	public static final String flag_enable = "1";

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSuperId() {
		return superId;
	}

	public void setSuperId(String superId) {
		this.superId = superId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String getIntroduce() {
		return introduce;
	}

	public void setIntroduce(String introduce) {
		this.introduce = introduce;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public int getRank() {
		return rank;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}
}
