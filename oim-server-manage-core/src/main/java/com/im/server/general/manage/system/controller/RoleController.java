package com.im.server.general.manage.system.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.im.server.general.common.bean.system.Role;
import com.im.server.general.common.bean.system.RoleMenu;
import com.im.server.general.common.data.system.RoleQuery;
import com.im.server.general.manage.common.annotation.PermissionMapping;
import com.im.server.general.manage.system.service.RoleService;
import com.onlyxiahui.common.message.result.ResultMessage;
import com.onlyxiahui.general.annotation.parameter.Define;
import com.onlyxiahui.general.annotation.parameter.RequestParameter;
import com.onlyxiahui.im.message.data.PageData;
import com.onlyxiahui.query.page.DefaultPage;

/**
 * date 2018-06-04 14:59:44<br>
 * description
 * 
 * @author XiaHui<br>
 * @since
 */
@RestController
@RequestMapping("/manage/system")
public class RoleController {

	@Autowired
	RoleService roleService;

	@ResponseBody
	@RequestParameter
	@PermissionMapping(name = "角色列表", key = "/manage/system/role/list", superKey = "system", type = PermissionMapping.Type.menu)
	@RequestMapping(method = RequestMethod.POST, value = "/role/list")
	public Object list(HttpServletRequest request,
			@Define("roleQuery") RoleQuery roleQuery,
			@Define("page") PageData page) {
		ResultMessage rm = new ResultMessage();
		try {

			DefaultPage defaultPage = new DefaultPage();
			defaultPage.setPageNumber(page.getPageNumber());
			defaultPage.setPageSize(page.getPageSize());
			List<Role> list = roleService.queryList(roleQuery, defaultPage);
			rm.put("list", list);
			rm.put("page", defaultPage);

		} catch (Exception e) {
			e.printStackTrace();
			rm.addError("500", "系统异常");
		}
		return rm;
	}

	@ResponseBody
	@RequestParameter
	@PermissionMapping(name = "编辑角色", key = "/manage/system/role/addOrUpdate", superKey = "/manage/system/role/list")
	@RequestMapping(method = RequestMethod.POST, value = "/role/addOrUpdate")
	public Object addOrUpdate(HttpServletRequest request,
			@Define("role") Role role,
			@Define("menuIds") List<String> menuIds) {
		ResultMessage rm = new ResultMessage();
		try {
			roleService.addOrUpdate(role, menuIds);
			rm.put("role", role);
			rm.put("menuIds", menuIds);
		} catch (Exception e) {
			e.printStackTrace();
			rm.addError("500", "系统异常");
		}
		return rm;
	}

	@ResponseBody
	@RequestParameter
	@PermissionMapping(name = "编辑角色", key = "/manage/system/role/get", superKey = "/manage/system/role/get")
	@RequestMapping(method = RequestMethod.POST, value = "/role/get")
	public Object get(HttpServletRequest request,
			@Define("id") String id) {
		ResultMessage rm = new ResultMessage();
		try {
			Role role = roleService.getRole(id);
			rm.put("role", role);
		} catch (Exception e) {
			e.printStackTrace();
			rm.addError("500", "系统异常");
		}
		return rm;
	}

	@ResponseBody
	@RequestParameter
	@PermissionMapping(name = "删除角色", key = "/manage/system/role/delete", superKey = "/manage/system/role/list")
	@RequestMapping(method = RequestMethod.POST, value = "/role/delete")
	public Object delete(HttpServletRequest request,
			@Define("id") String id) {
		ResultMessage rm = new ResultMessage();
		try {
			roleService.delete(id);
		} catch (Exception e) {
			e.printStackTrace();
			rm.addError("500", "系统异常");
		}
		return rm;
	}

	
	@ResponseBody
	@RequestParameter
	@PermissionMapping(name = "所有角色列表", key = "/manage/system/role/allList", superKey = "system", type = PermissionMapping.Type.menu)
	@RequestMapping(method = RequestMethod.POST, value = "/role/allList")
	public Object allList(HttpServletRequest request) {
		ResultMessage rm = new ResultMessage();
		try {
			List<Role> list = roleService.allList();
			rm.put("list", list);
		} catch (Exception e) {
			e.printStackTrace();
			rm.addError("500", "系统异常");
		}
		return rm;
	}
	
	@ResponseBody
	@RequestParameter
	@PermissionMapping(name = "角色权限", key = "/manage/system/role/roleMenuList", superKey = "/manage/system/role/list")
	@RequestMapping(method = RequestMethod.POST, value = "/role/roleMenuList")
	public Object roleMenuList(HttpServletRequest request,
			@Define("id") String id) {
		ResultMessage rm = new ResultMessage();
		try {
			List<RoleMenu> list = roleService.getRoleMenuListByRoleId(id);
			rm.put("list", list);
		} catch (Exception e) {
			e.printStackTrace();
			rm.addError("500", "系统异常");
		}
		return rm;
	}
}
